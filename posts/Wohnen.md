---
layout: page
title: Wohnen
sidebar_link: true
category: Infrastruktur
---

### Wohngemeinschaft

[Wikipedia](https://de.wikipedia.org/wiki/Wohngemeinschaft)

### Wohnungsbaugenossenschaft

[Wikipedia](https://de.wikipedia.org/wiki/Wohnungsbaugenossenschaft)

### Mietshäuser Syndikat

[Webseite des Mietshäuser Syndikats](https://www.syndikat.org/de/)
[Wikipedia](https://de.wikipedia.org/wiki/Mietsh%C3%A4user_Syndikat)
