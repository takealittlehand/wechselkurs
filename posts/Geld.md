---
layout: page
title: Geld
sidebar_link: true
category: Infrastruktur
---

### Regionalgeld
![](img/foss/regiogeld.png "Regionalgeld")
Regionalgelder sind Komplementärwährungen die von Privatpersonen oder Vereinen herausgegeben werden. Sie werden nur in bestimmten Regionen angenommen - daher der Name - und sollen so die lokale Wirtschaft fördern. In der Regel sind sie 1:1 durch die nationale Währung gedeckt, viele nutzen zudem eine Umlaufsicherung (siehe Freigeld).

[Dachverband mehrerer Regiogeld Initativen](http://regionetzwerk.blogspot.de/)
[Wikipedia](https://de.wikipedia.org/wiki/Regionalgeld)

### Umlaufgesichertes Geld

Umlaufgesichertes Geld (auch "Freigeld") bezeichnet Währungen die mit der Zeit ihren Wert verlieren. Was zum Anfang des Jahres ein 100er Schein ist wäre am Ende ein 90er Schein. So wird ein Anreiz geschaffen das Geld schnell wieder auszugeben anstatt zu horten. Damit verteilt sich der Reichtum gerechter und es gibt kein Zinseinkommen.

[Wikipedia](https://de.wikipedia.org/wiki/Umlaufgesichertes_Geld)

### Kryptographische Währungen
![](img/foss/bitcoin.png "Bitcoin")
Bitcoin ist ein digitales Zahlungsmittel das ohne zentrale Instanz auskommt. Die Menge aller Bitcoins ist vordefiniert und deshalb unabhängig von politischen Einfluss.

[Wikipedia](https://de.wikipedia.org/wiki/Kryptow%C3%A4hrung)

