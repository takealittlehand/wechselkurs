---
layout: page
title: Wechselkurs
---

In allen Bereichen des Lebens gibt es Alternativen die einem mehr Freiheit bieten. Dem Einzelnen individuell und unmittelbar, der Gemeinschaft umfassend und auf lange Sicht. Wechselkurs ist eine Liste von eben solchen Projekten und Methoden. Ob du nun in eine Wohngemeinschaft ziehst, die Bank wechselst oder dich mit freier Software beschäftigst - all diese Alternativen unterstützen sich indirekt gegenseitig.

Ziel ist es in Gemeinden, Städten, Vierteln, Freundeskreisen "Wechsel-Kurse" abzuhalten, in denen Alle allen Anderen als Experten dienen. Wer sich mit einem Thema auskennt, kann den anderen hier beim Wechseln helfen. Und wenn es lokal noch keine Möglichkeiten gibt, können zusammen Geschenkläden, Regionalgelder, Fahrgemeinschaften und Sonstiges geschaffen werden.

Die Inspiration für diese Seite stammt von [Prism-Break](https://prism-break.org/) und [Droid-Break](https://droid-break.info/), welche freie Alternativen zu kommerzieller Software sammeln.

Wenn etwas fehlt und du einen neuen Eintrag einreichen willst, dann erstell bitte einen Pull Request auf [Github](https://github.com/wechselkurs/wechselkurs.github.io).
