---
layout: page
title: Banken
sidebar_link: true
category: Dienstleistung
---

### Soziale Banken
![](img/foss/kroetenwanderung.png "Krötenwanderung")
[Bankenwechsel "Krötenwanderung"](http://www.attac.de/kampagnen/bankwechsel/worum-geht-es/)

### Islamisches Bankwesen
Die Schari'a verbietet Zinsen, Spekulation und Glücksspiel. Daher werden andere Instrumente angewendet, zum Beispiel eine Beteiligungsfinanzierung oder zinslose Anleihen.

[Wikipedia](https://de.wikipedia.org/wiki/Islamisches_Bankwesen)
