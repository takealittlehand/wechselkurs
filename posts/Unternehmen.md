---
layout: page
title: Unternehmen
sidebar_link: true
category: Produktion
---

### Kollektivbetrieb

Für Kollektivbetriebe gibt es keine konkrete Definition. Ziel ist ohne Herrschaftsstrukturen gemeinsam arbeiten zu können. Dazu gehören gemeinsamer Besitz des Unternehmens, ablehnen eines Chefs und Experimente mit gleichem Lohn für alle.

[Berliner Kollektivbetriebe](http://www.kollektiv-betriebe.org/)

### Genossenschaft
![](img/foss/eg.png "e.G.")
Genossenschaften sind in Deutschland, Österreich, Schweiz und Europa ("Europäische Genossenschaft") Rechtsformen. Sie sind Zusammenschlüsse zum Wohl der Mitglieder, brauchen keinen Chef und alle Genossenschaftler ist die Möglichkeit zur Mitsprache garantiert. So haben soziale und ökologische Ziele oft mehr Gewicht als in anderen Rechtsformen. Es ist anzumerken, dass die demokratischen Elemente nur weich definiert sind und in größeren G. oft ausgehebelt werden. Hierarchische Strukturen wie Vorstand und Aufsichtsrat sind hingegen vorgeschrieben, ebenso die Mitgliedschaft in einem Genossenschaftsverband (Situation in Deutschland).

[Wikipedia](https://de.wikipedia.org/wiki/Genossenschaft)
