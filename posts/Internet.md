---
layout: page
title: Internet
sidebar_link: true
category: Infrastruktur
---

### Freifunk
![](img/foss/freifunk.png "Freifunk")
Der eigene Internetanschluss wird durch Wlan öffentlich und kostenlos zur Verfügung gestellt. Dadurch, dass die einzelnen Router untereinander vernetzt sind entsteht ein eigenes Netzwerk.

[Initiative Freifunk](https://freifunk.net/)
