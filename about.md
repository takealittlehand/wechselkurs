---
layout: page
title: Über
sidebar_custom_link: true
---

## Lizenz

Diese Seite steht unter der Lizenz Attribution-ShareAlike 3.0 Unported (CC BY-SA 3.0). Das bedeutet, du darfst die Arbeit kopieren, verteilen, verändern und anpassen, selbst für kommerzielle Zwecke, sofern du die Urheber nennst und das neue Werk nur unter der selben Lizenz verbreitest.

Das Design der Seite basiert auf dem [Hideout Theme](https://github.com/fongandrew/hydeout) für [Jekyll](https://jekyllrb.com/) und steht unter der MIT Lizenz.

## Quellen

Soweit nicht anders genannt sind alle Symbole aus Wikimedia Commons entnommen.

Link kaputt? Nimm Kontakt auf oder forke das Projekt auf Github.

Medium |	Quelle |	Lizenz
---|---|---
F-Droid Logo |	https://commons.wikimedia.org/wiki/File:F-Droid_Logo_2.svg |	CC-SA 3.0 unported
OSI Logo |	http://opensource.org/files/garland_logo.png |	CC-BY 2.5 Generic
Krötenwanderung Logo |	http://www.attac.de/fileadmin/user_upload/Kampagnen/mym/gfx/Eurokroete.svg |	unbekannt

## Kontakt

Im Moment gibt es noch keine Support Mail, du kannst aber über das Projekt auf GitHub Kontakt aufnehmen.

## Spenden

Diese Seite ist ein Freizeitprojekt. Dadurch, dass sie von GitHub Pages gehosted wird entstehen uns keine Kosten.
